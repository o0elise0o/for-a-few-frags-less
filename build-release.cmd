@echo off
cls
cd .\source\
call .\build-acs-only.cmd
call .\build-wad-only.cmd
cd ..
echo.
echo ____
echo ____[Building Release Archive]
echo.
echo ____[Removing Old Zip]
echo.
del FaFFL.zip
echo.
echo ____[Creating New Zip]
7z a -tzip FaFFL.zip @release-include-list.txt -xr@release-exclude-list.txt
echo.
echo ____[Finalizing]
echo.
move "./FaFFL.zip" "./release/"
echo.
echo Release Zip file located in ./Release/
echo.
echo _
echo ___
echo _____
echo [Done]___
echo.
echo.
pause