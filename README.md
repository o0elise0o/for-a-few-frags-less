# For a Few Frags Less

For a Few Frags Less, a demake sequel to Fistful of Frags.

# Building:

## Full Release:

#### ./build-release.cmd,
Walks you through the complete building process, generating final .zip file for distribution.

## Build Stages:

#### ./source/build-full-no-test.cmd,
Compiles the ACS script, and generates the .pk3.

#### ./source/build-full-test.cmd,
Compiles the ACS script, generates the .pk3, and starts the included Zandronum engine with faffl.pk3 loaded, to map TEST01.

#### ./source/build-acs-only.cmd,
Compiles the ACS script only.

#### ./source/build-wad-only.cmd,
Generates just the .pk3 file, from the faffl/ subfolder(s).

#### ./source/build-test-only,cmd,
Starts the included Zandronum engine with faffl.pk3 loaded, to map TEST01.