Actor Cowboy : PlayerPawn
{
  Player.DisplayName "Cowboy"
  //
  Player.MaxHealth 200
  Player.MugShotMaxHealth 200
  Player.ForwardMove 0.5, 0.5
  Player.SideMove 0.5, 0.5
  Player.JumpZ 8.0
}

Actor Gunslinger : Cowboy
{
  Player.DisplayName "Gunslinger"
  //
  Player.MaxHealth 200
  Player.MugShotMaxHealth 200
  Player.ForwardMove 0.5, 0.5
  Player.SideMove 0.5, 0.5
  Player.JumpZ 8.0
  //
  Player.StartItem "BrassKnuckles"
  Player.WeaponSlot 1, BrassKnuckles
  Player.WeaponSlot 2, Deringers, Lemat
  Player.WeaponSlot 3, Shotgun, Coachgun
  Player.WeaponSlot 4, BoltActionRifle, YellowboyRifle
}

Actor Ranger : Cowboy
{
  Player.DisplayName "Ranger"
  //
  Player.MaxHealth 200
  Player.MugShotMaxHealth 200
  Player.ForwardMove 0.5, 0.5
  Player.SideMove 0.5, 0.5
  Player.JumpZ 8.0
  //
  Player.StartItem "Knife"
  Player.WeaponSlot 1, Knife
  Player.WeaponSlot 2, Deringers, Lemat
  Player.WeaponSlot 3, Shotgun, Coachgun
  Player.WeaponSlot 4, BoltActionRifle, YellowboyRifle
}

Actor Bandito : Cowboy
{
  Player.DisplayName "Bandito"
  //
  Player.MaxHealth 200
  Player.MugShotMaxHealth 200
  Player.ForwardMove 0.5, 0.5
  Player.SideMove 0.5, 0.5
  Player.JumpZ 8.0
  //
  Player.StartItem "Machete"
  Player.WeaponSlot 1, Machete
  Player.WeaponSlot 2, Deringers, Lemat
  Player.WeaponSlot 3, Shotgun, Coachgun
  Player.WeaponSlot 4, BoltActionRifle, YellowboyRifle
  Player.WeaponSlot 5, Molotov, Dynamite
}

Actor Native : Cowboy
{
  Player.DisplayName "Native American"
  //
  Player.MaxHealth 200
  Player.MugShotMaxHealth 200
  Player.ForwardMove 0.5, 0.5
  Player.SideMove 0.5, 0.5
  Player.JumpZ 8.0
  //
  Player.StartItem "Axe"
  Player.WeaponSlot 1, Axe
  Player.WeaponSlot 2, Deringers, Lemat
  Player.WeaponSlot 3, Shotgun, Coachgun
  Player.WeaponSlot 4, Bow, YellowboyRifle
}