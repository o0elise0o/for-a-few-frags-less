@echo off
echo.
echo ____
echo ____[Building ACS]
echo.
.\bin\acc.exe .\faffl\acs\game.acs .\faffl\acs\game.o
.\bin\acc.exe .\faffl\acs\axe.acs .\faffl\acs\axe.o
echo.
echo ____[Done]
pause