@echo off
echo.
echo ____
echo ____[Building WAD/PK3]
echo.
del FaFFL.pk3
echo.
7z a -tzip FaFFL.zip @build-include-list.txt -xr@build-exclude-list.txt
ren FaFFL.zip FaFFL.pk3
echo.
echo ____[Done]
pause